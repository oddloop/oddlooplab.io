{-|
MillerRabin.hs : a module implementing the Miller-Rabin primality test.
version 0.1 (November 10, 2018)

Copyright (C) 2018 Alexander Gosselin (alex@oddloop.ca)

This is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>
|-}

{-# LANGUAGE ScopedTypeVariables #-}

module MillerRabin (powMod, millerRabin, millerRabinIO) where

import Control.Arrow (first)
import Data.Function (fix)
import System.Random (Random, RandomGen, randomR, getStdRandom)

-- Modular exponentiation: powMod x k m = xᵏ (mod m).
powMod :: forall a. Integral a => a -> a -> a -> a
powMod _ _ 1 = 0
powMod _ 0 _ = 1
powMod x 1 m = x `mod` m
powMod x k m = go x k 1
  where
    go :: a -> a -> a -> a
    go x 0 r = r
    go x k r = go x' k' r'
      where
        x' = x*x `mod` m
        k' = k `div` 2
        r' | even k    = r
           | otherwise = r*x `mod` m

takeRandomRs :: (Random a, RandomGen g) => Int -> (a, a) -> g -> ([a], g)
takeRandomRs n ival = go n
  where
    go 0 g = ([], g)
    go n g = first (x:) (go (n - 1) g')
      where
        (x, g') = randomR ival g

millerRabin :: forall a g. (Integral a, Random a, RandomGen g)
            => a -> Int -> g -> (Bool, g)
millerRabin n k g = (not (any witness bs), g')
  where
    (bs, g') = takeRandomRs k (2, n - 2) g
    (l, m) = go 0 (n - 1)
      where
        go x y | odd y     = (x, y) 
               | otherwise = go (x + 1) (y `div` 2)
    witness :: a -> Bool
    witness b
      | x == 1    = False 
      | otherwise = not . any (== n - 1) . take l
                    . iterate (\x -> x*x `mod` n) $ x
      where
        x = powMod b m n 

millerRabinIO :: (Integral a, Random a) => a -> Int -> IO Bool
millerRabinIO n k = getStdRandom (millerRabin n k)
