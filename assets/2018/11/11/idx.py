"""
idx.py: functions for loading and saving IDX files with NumPy
version 0.1 (November 11, 2018)

Copyright (C) 2018 Alexander Gosselin <alex@oddloop.ca>

This is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>
"""

import gzip
import numpy as np


def loadidx(filename: str) -> np.ndarray:
    with (
        gzip.open(filename, "rb")
        if filename.endswith("gz")
        else open(filename, "rb")
    ) as file:
        try:
            assert file.read(2) == b"\x00\x00"
            dtype = np.dtype(
                {
                    0x08: "uint8",
                    0x09: "int8",
                    0x0C: "int16",
                    0x0D: "int32",
                    0x0E: "float32",
                    0x0F: "float64",
                }[file.read(1)[0]]
            ).newbyteorder(">")
            shape = tuple(
                int.from_bytes(file.read(4), "big")
                for N in range(file.read(1)[0])
            )
            assert shape and all(shape)
        except (AssertionError, KeyError):
            raise IOError("invalid IDX file header")
        return np.frombuffer(file.read(), dtype).reshape(shape)


def saveidx(filename: str, array: np.ndarray) -> None:
    with (
        gzip.open(filename, "wb")
        if filename.endswith(".gz")
        else open(filename, "wb")
    ) as file:
        try:
            file.write(b"\x00\x00")
            file.write(
                {
                    "uint8": b"\x08",
                    "int8": b"\x09",
                    "int16": b"\x0C",
                    "int32": b"\x0D",
                    "float32": b"\x0E",
                    "float64": b"\x0F",
                }[array.dtype.name]
            )
        except KeyError:
            raise ValueError(
                "dtype '%s' not supported by IDX file format" % array.dtype.name
            )
        assert array.shape and len(array.shape)
        file.write(len(array.shape).to_bytes(1, "big"))
        for dimension in array.shape:
            file.write(dimension.to_bytes(4, "big"))
        file.write(array.newbyteorder(">").tobytes())
