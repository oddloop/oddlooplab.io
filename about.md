---
layout: page
title: About
permalink: /about/
---

My name is Alex, and this is my personal blog. You can see my resume [here](/assets/resume_Alexander_Gosselin_BASc.pdf), and my public Gitlab repositories [here](https://gitlab.com/oddloop).

This site is built using [jekyll](https://jekyllrb.com/), [Org Mode](https://orgmode.org/), [pandoc](https://pandoc.org/), and [ox-pandoc](https://github.com/kawabata/ox-pandoc). Hosting is generously provided by [GitLab](https://gitlab.com/) through [GitLab Pages](https://about.gitlab.com/features/pages/).
